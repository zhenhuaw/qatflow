QATFlow: A Workflow Demo of TensorFlow Quantization-aware Training
================================================

One simple workflow to integrate training, [quantization-aware training][qat],
model converting, and TFLite serving together.


## Why & What

Aims to address the usage questions
([for example](https://groups.google.com/a/tensorflow.org/forum/#!topic/tflite/zK1NGLVEILk)).

This workflow demo partly originates from [Slim package][slim] where some
pre-trained official TensorFlow models are provided. The scripts have been
simplified such that they dependent on the TensorFlow package directly.

I mainly focus on ML system - not very famlilier with the high level APIs.
So if any misuse, please raise [issue](https://github.com/jackwish/qatflow/issues)
or open [PR](https://github.com/jackwish/qatflow/pulls). :)


## TensorFlow Dependent

As TensorFlow is moving fast, and 2.0 is sort of new design, the API used
by this workflow is not always the same. So we have different branches
which depend on different tensorflow version, the branch name is same as
the TensorFlow branch name it depends on.

Currently:
* [x] Branch [`master`](https://github.com/jackwish/qatflow/tree/master)
branch contains no tools because we'd like to make it as a landing page.
* [x] Branch [`r1.x`](https://github.com/jackwish/qatflow/tree/r1.x) is the only
supported branch currently. It has been tested aginst TensorFlow r1.12 and r1.14.
* [ ] Branch [`r2.0`](https://github.com/jackwish/qatflow/tree/r2.0)
is planed once stable TensorFlow 2.0 is released.

## NOTE

**Please switch to dedicated branches (as above) to check further.**


## License

[Apache License 2.0](LICENSE) as the [Slim tools][slim].


[slim]: https://github.com/tensorflow/models/tree/v1.12.0/research/slim "Slim"
[qat]: https://github.com/tensorflow/tensorflow/tree/v1.12.3/tensorflow/contrib/quantize "Quantization-aware Training"
